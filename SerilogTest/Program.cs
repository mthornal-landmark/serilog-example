﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Serilog;
using Serilog.Events;
using Serilog.Filters;
using SerilogMetrics;

namespace SerilogTest
{
    class Program
    {
        static void Main(string[] args)
        {
            

            var log = new LoggerConfiguration()                  
                .MinimumLevel.Verbose()                  
                .Enrich.WithProperty("Version", "1.0.0.0")
                .Enrich.WithMachineName()
                .Enrich.WithProcessId()
                .Enrich.WithThreadId()
                .WriteTo.ColoredConsole()
                .WriteTo.RollingFile("log.txt", LogEventLevel.Debug)
                .WriteTo.SplunkViaUdp("lmksplunk1.cloudapp.net", 514)
                .CreateLogger();

            log.Verbose("Verbose");
            log.Debug("Debug");
            log.Information("Information");
            log.Warning("Warning");
            log.Error("Error");
            log.Fatal("Fatal");

            var componentLog = log               
                .ForContext<Program>()
                .ForContext("environment", "uat")
                .ForContext("orderLineId", 4321)
                .ForContext("componentId", 1234)
                .ForContext("service", "getdata");                

            using (componentLog.BeginTimedOperation("timed operation"))
            {
                const string framework = "Serilog";
                componentLog.Information("Hello, {Framework}!", framework);
                var structuredMessage = new {message = "Hello", text = "World"};
                componentLog.Information("{@StructuredMessage}!", structuredMessage);                                
            }

            try
            {
                throw new Exception("Test Exception");
            }
            catch (Exception exception)
            {
                componentLog.Error(exception, "Bang!");
            }

            //var filteredLog = new LoggerConfiguration()
            //    .WriteTo.ColoredConsole()
            //    .Filter.ByExcluding(Matching.WithProperty<int>)

            // TODO: investigate
            componentLog.HealthCheck("HC", () => new HealthCheckResult(true, "healthy"));
        }
    }
}
